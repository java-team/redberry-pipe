package cc.redberry.pipe;

/**
 * This interface should be implemented along with the {@link OutputPort} interface to indicate that current output port
 * should be closed (by invoking {@link #close()} method) after downstream consumer (e.g. {@link
 * cc.redberry.pipe.blocks.Merger}) is closed. All standard blocks will automatically and thread-safe (the latter will
 * be applied only if output port is not marked as thread-safe using {@link ThreadSafe}) close current output port after
 * their own job is finished.
 *
 * Don't use this method to indicate that {@link InputPort} should be closed, because all input ports are by default
 * closable, and will be closed by putting null into them.
 */
public interface OutputPortCloseable<T> extends AutoCloseable, OutputPort<T> {
    /**
     * Close current output port and frees all resources used by it (threads, file streams, etc...).
     *
     * <p>Contract for implementation:<ul>
     *
     * <li>The implementation of this method must not throw an exceptions if port is already closed.</li>
     *
     * </ul></p>
     *
     * <p>If the implementation of this method does not fulfill this contract, than methods {@link
     * cc.redberry.pipe.blocks.Merger#close()}, {@link cc.redberry.pipe.blocks.ParallelProcessor#close()}, etc... will
     * have an unpredictable behavior.</p>
     *
     * <p>Use {@link cc.redberry.pipe.blocks.AbstractOutputPort} for proper synchronization.</p>
     */
    @Override
    void close();
}
