package cc.redberry.pipe.blocks;

/**
 * Interface for the object that can report buffer status (fullness and total number of objects being transferred)
 */
public interface BufferStatusProvider {
    /**
     * Returns snapshot of current buffer status
     */
    BufferStatus getStatus();
}
