package cc.redberry.pipe.blocks;

/**
 * Tuple storing input value passed to {@link cc.redberry.pipe.Processor} and result it has produced.
 *
 * @param <InputT>  input type
 * @param <OutputT> output type
 */
public final class InputOutputPair<InputT, OutputT> {
    public final InputT input;
    public final OutputT result;

    public InputOutputPair(InputT input, OutputT result) {
        this.input = input;
        this.result = result;
    }
}
