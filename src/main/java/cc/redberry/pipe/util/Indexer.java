package cc.redberry.pipe.util;

public interface Indexer<T> {
    long getIndex(T object);
}
