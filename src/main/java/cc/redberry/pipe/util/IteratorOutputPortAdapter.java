package cc.redberry.pipe.util;

import cc.redberry.pipe.OutputPort;

import java.util.Iterator;

public class IteratorOutputPortAdapter<T> implements OutputPort<T> {
    private Iterator<T> iterator;

    public IteratorOutputPortAdapter(Iterator<T> iterator) {
        this.iterator = iterator;
    }

    public IteratorOutputPortAdapter(Iterable<T> iterable) {
        this.iterator = iterable.iterator();
    }

    @Override
    public synchronized T take() {
        if (iterator == null)
            return null;
        if (!iterator.hasNext())
            return null;
        return iterator.next();
    }
}
