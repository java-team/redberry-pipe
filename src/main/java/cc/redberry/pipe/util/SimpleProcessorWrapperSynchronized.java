/*
 * cc.redberry.pipe: java library for implementation of concurrent pipelines
 * Copyright (c) 2010-2012
 * Bolotin Dmitriy <bolotin.dmitriy@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cc.redberry.pipe.util;

import cc.redberry.pipe.OutputPort;
import cc.redberry.pipe.OutputPortCloseable;
import cc.redberry.pipe.Processor;

/**
 * This class is used by {@link cc.redberry.pipe.CUtils#wrap(cc.redberry.pipe.OutputPort, cc.redberry.pipe.Processor)}
 * method.
 *
 * @param <InputT>  type of consumed object
 * @param <OutputT> type of produced object
 * @author Bolotin Dmitriy (bolotin.dmitriy@gmail.com)
 */
public class SimpleProcessorWrapperSynchronized<InputT, OutputT> implements OutputPortCloseable<OutputT> {
    private final OutputPort<? extends InputT> innerPort;
    private final Processor<? super InputT, ? extends OutputT> processor;

    public SimpleProcessorWrapperSynchronized(OutputPort<? extends InputT> innerPort, Processor<? super InputT, ? extends OutputT> processor) {
        this.innerPort = innerPort;
        this.processor = processor;
    }

    @Override
    public OutputT take() {
        InputT input = innerPort.take();
        if (input == null)
            return null;
        OutputT result;
        synchronized (processor) {
            result = processor.process(input);
        }
        return result;
    }

    @Override
    public void close() {
        if (innerPort instanceof OutputPortCloseable)
            ((OutputPortCloseable) innerPort).close();
    }
}

