package cc.redberry.pipe.util;

import cc.redberry.pipe.blocks.BufferStatus;
import cc.redberry.pipe.blocks.BufferStatusProvider;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Universal periodic reporter
 */
public class StatusReporter extends Thread {
    private static int FULLNESS_SLOTS = 20;
    private static final DecimalFormat decFormat = new DecimalFormat("##.#");
    final int reportPeriod;
    final OutputStream outputStream;
    final ArrayList<StatusProvider> buffers = new ArrayList<>();

    public StatusReporter() {
        this(5000);
    }

    public StatusReporter(int reportPeriod) {
        this(reportPeriod, System.err);
    }

    /**
     * @param reportPeriod report period in milliseconds
     * @param outputStream stream to print report
     */
    public StatusReporter(int reportPeriod, OutputStream outputStream) {
        this.reportPeriod = reportPeriod;
        this.outputStream = outputStream;
        this.setDaemon(true);
    }

    public synchronized void addBuffer(String name, BufferStatusProvider buffer) {
        buffers.add(new BufferHolder(name, buffer));
    }

    public synchronized void addCustomProvider(StatusProvider provider) {
        buffers.add(provider);
    }

    public synchronized void addCustomProviderFromLambda(Supplier<Status> statusSupplier) {
        buffers.add(new StatusProvider() {
            volatile Status status;

            @Override
            public void updateStatus() {
                status = statusSupplier.get();
            }

            @Override
            public boolean isFinished() {
                return status.isFinished;
            }

            @Override
            public String getStatus() {
                return status.status;
            }
        });
    }

    @Override
    public void run() {
        try {
            while (true) {
                synchronized (this) {
                    // Updating status
                    buffers.forEach(StatusProvider::updateStatus);

                    // Removing all closed and empty buffers
                    buffers.removeIf(StatusProvider::isFinished);

                    // Stopping reporting thread if there are no mre buffers
                    if (buffers.isEmpty())
                        break;

                    // Putting report to the stream
                    // In single transaction, hoping this will be printed as single chunk of information
                    outputStream.write(
                            ("\n" + buffers.stream()
                                    .map(StatusProvider::getStatus)
                                    .collect(Collectors.joining("\n")) + "\n")
                                    .getBytes());

                    // Sleep until next cycle
                    Thread.sleep(reportPeriod);
                }
            }
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private final static class BufferHolder implements StatusProvider {
        final String bufferName;
        final BufferStatusProvider statusProvider;
        BufferStatus currentStatus, previousStatus;

        public BufferHolder(String bufferName, BufferStatusProvider statusProvider) {
            this.bufferName = bufferName;
            this.statusProvider = statusProvider;
        }

        @Override
        public void updateStatus() {
            previousStatus = currentStatus;
            currentStatus = statusProvider.getStatus();
        }

        @Override
        public boolean isFinished() {
            return currentStatus.isClosedAndEmpty();
        }

        @Override
        public String getStatus() {
            if (currentStatus == null)
                return "status unknown";

            int fullnessBlocks = (int) (currentStatus.fullness() * FULLNESS_SLOTS);
            String mainPart = bufferName + ": -" + (currentStatus.closed ? "X" : ">") + " [" +
                    new String(new char[fullnessBlocks]).replace("\0", "=") +
                    new String(new char[FULLNESS_SLOTS - fullnessBlocks]).replace("\0", " ") +
                    "] (" + currentStatus.size() + "/" + currentStatus.capacity + ") -> ";

            if (previousStatus == null)
                return mainPart;

            return mainPart + decFormat.format((currentStatus.takeCount - previousStatus.takeCount) * 1000.0 /
                    (currentStatus.timestamp - previousStatus.timestamp)) + " ops/s";
        }
    }

    public interface StatusProvider {
        /**
         * This method should get the information from underlying status source, and save it into internal state.
         */
        void updateStatus();

        /**
         * No status reporting required for this item.
         *
         * If true returned it will be removed from the reporters list.
         */
        boolean isFinished();

        /**
         * Return string status.
         */
        String getStatus();
    }

    public static final class Status {
        final String status;
        final boolean isFinished;

        public Status(String status, boolean isFinished) {
            this.status = status;
            this.isFinished = isFinished;
        }
    }
}
