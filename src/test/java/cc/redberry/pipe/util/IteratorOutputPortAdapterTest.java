package cc.redberry.pipe.util;

import cc.redberry.pipe.OutputPort;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static junit.framework.Assert.assertEquals;

public class IteratorOutputPortAdapterTest {
    @Test
    public void test1() throws Exception {
        List<String> strings = Arrays.asList(new String[]{"k1", "k2", "k3", "k4"});
        OutputPort<String> sPort = new IteratorOutputPortAdapter<String>(strings);
        for (String s : strings)
            assertEquals(s, sPort.take());
        assertEquals(null, sPort.take());
    }
}
